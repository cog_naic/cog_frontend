//import {useEffect, useState} from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {Container} from 'react-bootstrap';
import Home from './pages/Home';
import Members from './pages/Members';

//import logo from './logo.svg';


//import './App.css';

export default function App() {

return (
<>
  <Router>
    <Container>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/datas" element={<Members/>}/>
      </Routes>
    </Container>
  </Router>
</>
)
}