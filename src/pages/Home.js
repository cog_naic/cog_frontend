import Banner from "../components/banner"
import Highlights from "../components/highlights"
import { useLocation } from 'react-router-dom';

export default function Home() {
  const location = useLocation();
  const isHomePage = location.pathname === '/';

  if (isHomePage) {
    return (
      <>
        <div id="Homecontainer" style={{ borderRadius: '30px' }}>
          <div className="App">
            <Banner
              title="E COMMERCE PAGE"
              subtitle="BUY AND SELL"
              buttonText="BUY NOW REGRET LATER"
            />
          </div>
          <Highlights />
        </div>
      </>
    );
  }
}


