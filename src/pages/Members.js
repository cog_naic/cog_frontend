import React, { useState, useEffect } from 'react';

const Table = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchDataFromBackend();
  }, []);

  const fetchDataFromBackend = async () => {
    try {
      const response = await fetch('http://localhost:4000/data');
      //const response = await fetch(`${process.env.REACT_APP_API_URL}/data`);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const jsonData = await response.json();
      setData(jsonData);
      setLoading(false);
    } catch (error) {
      setError(error.message);
      setLoading(false);
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div>
      <h2>Data Table</h2>
      <table style={{ borderCollapse: 'collapse', width: '100%', border: '2px solid green' }}>
        <thead>
          <tr style={{ backgroundColor: 'green' }}>
            <th style={{ border: '1px solid green', padding: '8px' }}>ID</th>
            <th style={{ border: '1px solid green', padding: '8px' }}>Email</th>
            <th style={{ border: '1px solid green', padding: '8px' }}>First Name</th>
            <th style={{ border: '1px solid green', padding: '8px' }}>Middle Name</th>
            <th style={{ border: '1px solid green', padding: '8px' }}>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {data.map(item => (
            <tr key={item.id} style={{ backgroundColor: 'lightgreen' }}>
              <td style={{ border: '1px solid green', padding: '8px' }}>{item.id}</td>
              <td style={{ border: '1px solid green', padding: '8px' }}>{item.email}</td>
              <td style={{ border: '1px solid green', padding: '8px' }}>{item.firstName}</td>
              <td style={{ border: '1px solid green', padding: '8px' }}>{item.middleName}</td>
              <td style={{ border: '1px solid green', padding: '8px' }}>{item.lastName}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
