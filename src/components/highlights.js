import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const FrontPage = () => {
  return (
    <Container className="mt-3">
      <h1 className="text-center mb-4">Welcome to Our Church</h1>
      <Row xs={1} sm={2} md={4} lg={4} className="g-4">
        {/* Highlight 1 */}
        <Col>
          <div className="highlight blue-background">
            <h3>Join Us for Worship</h3>
            <p>Sunday Service: 10:00 AM</p>
            <p>Location: 123 Main Street, City</p>
            <a href="/worship" className="btn btn-primary">Learn More</a>
          </div>
        </Col>

        {/* Highlight 2 */}
        <Col>
          <div className="highlight blue-background">
            <h3>Community Events</h3>
            <p>Upcoming Event: Fellowship BBQ</p>
            <p>Date: July 15th, 2024</p>
            <a href="/events" className="btn btn-success">View Events</a>
          </div>
        </Col>

        {/* Highlight 3 */}
        <Col>
          <div className="highlight blue-background">
            <h3>Get Involved</h3>
            <p>Volunteer Opportunities Available</p>
            <a href="/get-involved" className="btn btn-primary">Join Us</a>
          </div>
        </Col>

        {/* Highlight 4 */}
        <Col>
          <div className="highlight blue-background">
            <h3>Stay Connected</h3>
            <p>Subscribe to our Newsletter</p>
            <a href="/newsletter" className="btn btn-primary">Subscribe</a>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default FrontPage;
